<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('action/editer_rubrique');
include_spip('action/editer_article');

define('_RUBRIQUEUR_DEUX_POINTS_SUBSTITUT', '%%DEUXPOINTS%%');

function formulaires_rubriqueur_charger_dist() {

	$langues_utilisees = liste_options_langues('var_lang');
	$langues = [];
	foreach ($langues_utilisees as $langue) {
		$langues[$langue] = spip_ucfirst(html_entity_decode($GLOBALS['codes_langues'][$langue]));
	}

	return [
		'rubrique_racine' => '',
		'rubriques'       => '',
		'numeroter'       => '',
		'creer_articles'  => '',
		'langue'          => _request('langue') ? _request('langue') : lire_config('langue_site'),
		'langues'         => $langues,
	];
}

function formulaires_rubriqueur_verifier_dist() {
	$retour = [];
	if (!_request('rubriques')) {
		$retour['rubriques'] = _T('champ_obligatoire');
	}
	$rubrique_racine = picker_selected(_request('rubrique_racine'), 'rubrique');
	$rubrique_racine = array_pop($rubrique_racine);
	if (!autoriser('creerrubriquedans', 'rubrique', $rubrique_racine)) {
		$retour['message_erreur'] = _T('rubriqueur:pas_autorise');
	}

	// confirmation intermédiaire
	if (!$retour && _request('confirmer') != 'on') {
		$data = _rubriqueur_parse_texte(_request('rubriques'), 'previsu');
		// les données calculées sont un tableau ? l'analyse yaml a réussi
		if (is_array($data)) {
			$previsu = '{{{' . _T('rubriqueur:apercu_import') . '}}}';
			if ((int)_request('rubrique_racine')) {
				$hierarchie = explode(',', calcul_hierarchie_in($rubrique_racine));
				array_shift($hierarchie);
				$chemin = [];
				foreach ($hierarchie as $id_rubrique) {
					$chemin[] = supprimer_numero(sql_getfetsel('titre', 'spip_rubriques', 'id_rubrique=' . $id_rubrique));
				}
				$previsu .= _T('rubriqueur:dans_la_rubrique') . ' {{' . join(' > ', $chemin) . '}}';
			} else {
				$previsu .= _T('rubriqueur:a_la_racine');
			}
			$langue = _request('langue');
			if (!$langue) {
				$langue = lire_config('langue_site');
			}
			$previsu .= _rubriqueur_traiter_rubrique($data, $rubrique_racine, 'previsu', 0, '', $langue, _request('numeroter'), _request('creer_articles'));
			$retour['previsu'] = $previsu;
			$retour['message_erreur'] = _T('rubriqueur:confirmer_import');
		} // sinon, on retourne le message d'erreur
		else {
			$retour['erreur_analyse'] = _T('rubriqueur:erreur_analyse') . "\n\n" . '{{' . $data . '}}';
			$retour['message_erreur'] = _T('rubriqueur:erreur_analyse');
		}
	}

	return $retour;
}

function formulaires_rubriqueur_traiter_dist() {
	$rubrique_racine = 0;
	if (_request('rubrique_racine')) {
		$rubrique_racine = picker_selected(_request('rubrique_racine'), 'rubrique');
		$rubrique_racine = array_pop($rubrique_racine);
	}
	$rubriques = _rubriqueur_parse_texte(_request('rubriques'));
	$langue = _request('langue');
	if (!$langue) {
		$langue = lire_config('langue_site');
	}

	_rubriqueur_traiter_rubrique($rubriques, $rubrique_racine, 'creer', 0, '', $langue, _request('numeroter'), _request('creer_articles'));

	// mettre à jour les status, id_secteur et profondeur
	include_spip('inc/rubriques');
	calculer_rubriques();
	propager_les_secteurs();

	return [
		'message_ok' => _T('rubriqueur:rubriques_creees'),
		'editable'   => false,
	];
}

function _rubriqueur_traiter_rubrique($rubriques, $id_parent = 0, $mode = 'creer', $profondeur = 0, $retour = '', $langue = '', $numeroter = '', $creer_articles = '') {
	if (!is_array($rubriques)) {
		return;
	}
	if (!is_array($numeroter)) {
		$numeroter = [];
	}

	$index_article = 1;
	$index_rubrique = 1;
	static $index_articles_rubriques = [];

	foreach ($rubriques as $key => $value) {
		if (!is_numeric($key)) {

			// Créer une rubrique
			$index_articles_rubriques[$profondeur + 1] = 1;
			$titre = (in_array('rubriques', $numeroter) ? ($index_rubrique * 10) . '. ' : '') . str_replace(_RUBRIQUEUR_DEUX_POINTS_SUBSTITUT, ':', $key);
			$texte = '';
			if (strpos($titre, '||') !== false) {
				$split = explode('||', $titre);
				$titre = trim(array_shift($split));
				$texte = trim(implode($split));
				$texte = str_replace('\n', "\n", $texte);
			}
			if ($mode == 'creer') {
				$id_rubrique = rubrique_inserer($id_parent, ['titre' => $titre, 'texte' => $texte, 'lang' => $langue, 'langue_choisie' => 'oui']);
			} else {
				$id_rubrique = null;
				$retour .= "\n" . '-' . str_repeat('*', $profondeur) . '* <span class="rubrique">' . $titre . ($texte ? '<span class="rubrique__texte">' . $texte . '</span>' : '') . '</span>';
			}
			$retour .= _rubriqueur_traiter_rubrique($value, $id_rubrique, $mode, $profondeur + 1, '', $langue, $numeroter, $creer_articles);
			$index_rubrique++;

			if ($creer_articles) {
				// Ajouter un article par défaut
				$index = isset($index_articles_rubriques[$profondeur + 1]) ? $index_articles_rubriques[$profondeur + 1] : 1;
				$titre = (in_array('articles', $numeroter) ? ($index * 10) . '. ' : '') . str_replace(_RUBRIQUEUR_DEUX_POINTS_SUBSTITUT, ':', $key);
				if ($mode == 'creer') {
					article_inserer($id_rubrique, ['titre' => $titre, 'lang' => $langue, 'langue_choisie' => 'oui', 'statut' => 'publie']);
				} else {
					$retour .= "\n" . '-' . str_repeat('*', $profondeur + 1) . '* <span class="article">' . $titre . '</span>';
				}
			}

		} else {

			// Créer un article
			$titre = (in_array('articles', $numeroter) ? ($index_article * 10) . '. ' : '') . str_replace(_RUBRIQUEUR_DEUX_POINTS_SUBSTITUT, ':', $value);
			$texte = '';
			if (strpos($titre, '||') !== false) {
				$split = explode('||', $titre);
				$titre = trim(array_shift($split));
				$texte = trim(implode($split));
				$texte = str_replace('\n', "\n", $texte);
			}
			if ($mode == 'creer') {
				article_inserer($id_parent, ['titre' => $titre, 'texte' => $texte, 'lang' => $langue, 'langue_choisie' => 'oui', 'statut' => 'publie']);
			} else {
				$retour .= "\n" . '-' . str_repeat('*', $profondeur) . '* <span class="article">' . $titre . ($texte ? '<span class="article__texte">' . $texte . '</span>' : '') . '</span>';
			}
			$index_article++;
			$index_articles_rubriques[$profondeur] = $index_article;

		}
	}

	return $retour;
}

function _rubriqueur_parse_texte($texte, $mode = 'creer', $indentation = '  ') {
	require_spip('inc/yaml');

	// transformer le texte en YAML pour pouvoir le décoder

	// remplacer les : des titres par un marqueur qu'on supprimera ensuite
	$yaml = str_replace(':', _RUBRIQUEUR_DEUX_POINTS_SUBSTITUT, $texte);
	// ajouter : à la fin de chaque ligne pour indiquer les sous rubriques
	$yaml = preg_replace('#^(\s*)([^\r\n]+).*$#m', '$1$2:', $yaml);
	// supprimer les : sur les lignes d'articles (pas d'enfants)
	$yaml = preg_replace('#^(\s*)(\-\s)([^:\r\n]+)(:).*$#m', '$1- $3', $yaml);

	// retourner un tableau en cas de succès, une chaine en cas d'erreur
	try {
		$retour = yaml_decode($yaml);
	} catch (Exception $e) {
		$retour = $e->getMessage();
	}

	return $retour;
}
