<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rubriqueur-rubriqueur?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_la_racine' => 'Na raiz do site',
	'apercu_import' => 'Aqui estão as seções que serão criadas:',

	// B
	'bouton_creer' => 'Criar as seções',

	// C
	'confirmer_import' => 'Confirmar a importação',
	'creer_articles' => 'Criar uma matéria (com o status publicado) em cada seção, com o mesmo nome da seção',

	// D
	'dans_la_rubrique' => 'Na seção:',

	// E
	'erreur_analyse' => 'Erro na análise das seções',

	// L
	'langue' => 'Idioma:',

	// N
	'numeroter' => 'Enumerar:',
	'numeroter_articles' => 'as matérias',
	'numeroter_explication' => 'Enumerar automaticamente de 10 em 10',
	'numeroter_rubriques' => 'as seções',

	// P
	'pas_autorise' => 'Você não tem autorização para modificar esta seção',

	// R
	'rubrique_racine' => 'Na seção:',
	'rubrique_racine_explications' => 'Escolher a seção na qual as seções abaixo serão criadas, se não as seções serão criadas na raiz do site.',
	'rubriques' => 'Seções a serem criadas:',
	'rubriques_creees' => 'As seções foram criadas',
	'rubriques_explications' => 'Uma linha por seção<br>
Dois espaços no começo da linha para criar uma subseção ou uma matéria.<br>
As matérias serão identificadas por um traço e um espaço na frente do título.<br><br>
Exemplo:
<pre>Seção 1
  Subseção 1.1
    - Título da matéria || Texto da matéria \\n com quebra de linha
  Subseção 1.2
    - Matéria: uma primeira
    - Matéria: uma segunda
Seção 2
  Subseção 2.1
    Sub subseção 2.1.1
      - Uma outra matéria
      - E mais uma outra
    Sub subseção 2.1.2, sem matérias
  Subseção 2.2
  Subseção 2.3</pre>',
	'rubriqueur_titre' => 'Criar seções'
);
