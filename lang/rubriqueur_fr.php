<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/rubriqueur.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_la_racine' => 'À la racine du site',
	'apercu_import' => 'Voilà les rubriques qui vont être créées :',

	// B
	'bouton_creer' => 'Créer les rubriques',

	// C
	'confirmer_import' => 'Confirmer l’import',
	'creer_articles' => 'Créer un article (au statut publié) dans chaque rubrique, qui portera le même nom que le rubrique',

	// D
	'dans_la_rubrique' => 'Dans la rubrique :',

	// E
	'erreur_analyse' => 'Erreur dans l’analyse des rubriques',

	// L
	'langue' => 'Langue :',

	// N
	'numeroter' => 'Numéroter :',
	'numeroter_articles' => 'les articles',
	'numeroter_explication' => 'Numéroter automatiquement de 10 en 10',
	'numeroter_rubriques' => 'les rubriques',

	// P
	'pas_autorise' => 'Vous n’êtes pas autorisé•e à modifier cette rubrique',

	// R
	'rubrique_racine' => 'Dans la rubrique :',
	'rubrique_racine_explications' => 'Choisir la rubrique dans laquelle les rubriques ci dessous seront créées, sinon les rubriques seront créées à la racine du site.',
	'rubriques' => 'Rubriques à créer :',
	'rubriques_creees' => 'Les rubriques ont été créées',
	'rubriques_explications' => 'Une ligne par rubrique
<br>Deux espaces en début de ligne pour créer une sous rubrique ou un article.
<br>Les articles sont identifiés par un tiret et un espace devant le titre.
<br><br>Pour insérer du texte dans un article ou une rubrique, ajouter || après le titre pour le séparer du texte
<br><br>Exemple :
<pre>Rubrique 1
  Sous rubrique 1.1
    - Titre article || Texte article \\n avec retour à la ligne
  Sous rubrique 1.2
    - Article : un premier
    - Article : un deuxième
Rubrique 2
  Sous rubrique 2.1
    Sous sous rubrique 2.1.1
      - Un autre article
      - Et encore un autre
    Sous sous rubrique 2.1.2, sans article
  Sous rubrique 2.2
  Sous rubrique 2.3</pre>',
	'rubriqueur_titre' => 'Créer des rubriques'
);
