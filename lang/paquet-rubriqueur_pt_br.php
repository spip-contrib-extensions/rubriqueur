<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rubriqueur-paquet-xml-rubriqueur?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'rubriqueur_description' => 'Criar rapidamente uma hierarquia de seções e de matérias a partir de uma lista inserida num formulário simples',
	'rubriqueur_nom' => 'Rubriqueur',
	'rubriqueur_slogan' => 'Criar rapidamente seções e matérias'
);
