# Changelog

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/)

## [2.2.4] - 2022-05-25
### Added
- Possibilité d'insérer du texte dans les articles ou les rubriques en séparant le titre et le texte avec || (les séquances \n sont transformées en fins de ligne)

## [2.2.3] - 2022-05-25
### Added
- Compatibilité SPIP 4.1
### Fixed
- Taille des icones en background de la prévisualisation
